# Smart Filament Holder

I hate when a 30 hour print fails because I lack the last 50g of filament. With that in mind I am
building myself a filament holder for at least 4 spools that can keep track of the weight of the
spools and how much filament I have left. This holder will communicate via WiFi with my PC and
interface with custom-built software, as well as displaying the data on an embedded display. 

![](doc/screenshot.png)

## 3D Parts  

This contains the parts and STLs for the physical part of the contraption

## Firmware

The firmware for the microcontroller on the hardware.

## Hardware

The eagle CAD files and gerber exports for the hardware

## Software

The cross-platform software to communicate with the holder built in Qt.