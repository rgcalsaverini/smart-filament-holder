import threading
import time
from socket import *
import datetime
import sys

def get_hostname():
    s = socket(AF_INET, SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    host = str(s.getsockname()[0])
    s.close()
    return host


def get_broadcast_ip():
    host = get_hostname().split('.')
    return '.'.join([str(i) for i in [host[0], host[1], host[2], 255]])


class PingerThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while not found_client:
            cs = socket(AF_INET, SOCK_DGRAM)
            cs.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
            cs.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
            cs.sendto(payload, (get_broadcast_ip(), broadcast_port))
            cs.close()
            time.sleep(5)


def handle_client_connection(client_socket, address):
    response = client_socket.recv(1024)
    rec_magic = [int(r) for r in response[0:8]]
    if rec_magic != magic:
        return
    global found_client
    port = int(response[8:].decode('latin1'))
    found_client = (address[0], port)
    print(f'Found client {address[0]}:{port}')
    client_socket.close()


def command(remote, cmd):
    remote.send(cmd.encode('utf-8'))
    return remote.recv(1024).decode('utf-8')


def get_value(remote):
    res = command(remote, 'get spool-weight')
    res = int(res)
    if res & 0x80000000:
        res -= 0x100000000
    return res


magic = [25, 204, 170, 132, 141, 99, 168, 172]
host = get_hostname()
response_port = 5000
broadcast_port = 5001

while True:
    try:
        server = socket(AF_INET, SOCK_STREAM)
        server.bind(('0.0.0.0', response_port))
        break
    except OSError:
        response_port += 1

server.listen(2)

payload = b''.join([
    *[n.to_bytes(1, byteorder='big') for n in magic],
    len(host).to_bytes(1, byteorder='big'),
    len(str(response_port)).to_bytes(1, byteorder='big')
])

payload = payload + host.encode('latin1') + str(response_port).encode('latin1')

found_client = False

pinger = PingerThread()
pinger.start()

client_sock, address = server.accept()
client_handler = threading.Thread(
    target=handle_client_connection,
    args=(client_sock, address)
)
client_handler.start()

while not found_client:
    time.sleep(0.5)

server.close()

iot = client_socket = socket()
iot.connect(found_client)

print('Connected')

try:
    while True:
        value = int(command(iot, 'get lc-value-average')) - 2**32
        print(
            ''.join([datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S"), '\t', str(value)])
        )
        sys.stdout.flush()
        time.sleep(10)
except KeyboardInterrupt:
    pass
iot.close()
