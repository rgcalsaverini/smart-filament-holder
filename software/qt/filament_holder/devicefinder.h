#ifndef CPPUDP_DEVICEFINDER_H
#define CPPUDP_DEVICEFINDER_H

#include <iostream>
#include <string>


class DeviceFinder {
public:
    explicit DeviceFinder(uint16_t broadcast_port, std::string magic, uint16_t response_port = 6789);

    std::pair<std::string, uint16_t> find(uint16_t max_tries = 0);

private:
    void getIp();

    void getBroadcastIP();

    void sendBroadcast();

    void generateBroadcastPayload();

    void responseServer();

    static bool portInUse(uint16_t port);

private:
    uint16_t udp_port;
    uint16_t tcp_server_port;
    std::string own_ip;
    std::string broadcast_ip;
    std::string broadcast_payload;
    std::string magic_number;
    bool server_running;
    bool found;
    std::string target_host;
    uint16_t target_port;
};


#endif //CPPUDP_DEVICEFINDER_H
