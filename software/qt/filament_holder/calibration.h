#ifndef FILAMENT_HOLDER_CALIBRATION_H
#define FILAMENT_HOLDER_CALIBRATION_H


#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class CalibDialog; }
QT_END_NAMESPACE

class CalibrationDialog : public QDialog {
Q_OBJECT

public:
    explicit CalibrationDialog(QWidget *parent = nullptr);

    ~CalibrationDialog() override;

    void reset(uint8_t num);

public slots:
    void onTare();

    void onCalibrate();

    void onNameApply();

    void onRefApply();

    void onTabChange();

private:


private:
    Ui::CalibDialog *ui;
    uint8_t spool_num;
    std::string spool_str;
};
#endif //FILAMENT_HOLDER_CALIBRATION_H
