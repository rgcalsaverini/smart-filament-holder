#include "spoolslot.h"
#include "./ui_spoolslot.h"
#include "mainwindow.h"

SpoolSlot::SpoolSlot(QWidget *parent, uint8_t num) :
        QWidget(parent),
        slot_number(num),
        ui(new Ui::SpoolSlot) {
    ui->setupUi(this);

    // connect(ui->tare, SIGNAL(clicked(bool)), this, SLOT(onTare()));
    connect(ui->settings, SIGNAL(clicked(bool)), this, SLOT(onSettingsClick()));

//    QLabel *numLabel = new QLabel("X");
//    ui->iconContainer->layout()->addWidget(numLabel);
//    numLabel->move(50, 80);
    ui->numLabel->setText(std::to_string(num + 1).c_str());
    ui->name->setText("Spool " + QString::number(num + 1));
}

SpoolSlot::~SpoolSlot() {
    delete ui;
}

void SpoolSlot::updateWeight(std::string value) {
    ui->weight->setText(value.c_str());
}

void SpoolSlot::updateName(std::string name) {
    ui->name->setText(name.c_str());
}

void SpoolSlot::updateFilamentLeft(std::string value) {
    ui->filamentLeft->setText(value.c_str());
}
//
//void SpoolSlot::onTare() {
//    MainWindow *window = qobject_cast<MainWindow *>(parent());
////    window->test();
//    window->tare(slot_number);
//}
//void SpoolSlot::onSettingsClick() {
//    auto *window = qobject_cast<MainWindow *>(parent());
////    window->openSpoolSettings(slot_number);
//}
