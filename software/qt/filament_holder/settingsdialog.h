#ifndef FILAMENT_HOLDER_SETTINGSDIALOG_H
#define FILAMENT_HOLDER_SETTINGSDIALOG_H

#include <QDialog>
#include "mainwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class SettingsDialog : public QDialog {
Q_OBJECT

public:
    SettingsDialog(QWidget *parent = nullptr);

    ~SettingsDialog();

    void enableDeviceSettings(bool value);

    void reload();

public slots:

    void onSaveClick();

    void onDiscardClick();

    void onRestoreClick();

private:


private:
    Ui::Dialog *ui;
    bool device_settings_enabled;
};

#endif //FILAMENT_HOLDER_SETTINGSDIALOG_H
