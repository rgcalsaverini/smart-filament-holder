#include <QRegularExpressionValidator>
#include "calibration.h"
#include "./ui_calibration.h"
#include "mainwindow.h"

using std::string;

CalibrationDialog::CalibrationDialog(QWidget *parent)
        : QDialog(parent), ui(new Ui::CalibDialog) {
    ui->setupUi(this);
    setWindowTitle("Calibration");
    ui->weight->setValidator(new QRegularExpressionValidator(QRegularExpression("[0-9]{1,4}")));
    connect(ui->tare, SIGNAL(clicked(bool)), this, SLOT(onTare()));
    connect(ui->calibrate, SIGNAL(clicked(bool)), this, SLOT(onCalibrate()));
    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(onTabChange()));
    connect(ui->actionsName->button(QDialogButtonBox::Apply), SIGNAL(clicked(bool)), this, SLOT(onNameApply()));
    connect(ui->actionsRef->button(QDialogButtonBox::Apply), SIGNAL(clicked(bool)), this, SLOT(onRefApply()));

//    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(onTabChange()));


}

CalibrationDialog::~CalibrationDialog() {
    delete ui;
}

void CalibrationDialog::onTare() {
    ui->tare->setText("Wait...");
    setEnabled(false);
    auto *window = qobject_cast<MainWindow *>(parent());
    string offset_value = window->deviceCommand("get lc-value-average " + spool_str + " 10 0");
    window->deviceCommand("set lc-offset " + spool_str + " " + offset_value);
    ui->tare->setText("Tare");
    window->updateTick();
    setEnabled(true);
}

void CalibrationDialog::onCalibrate() {
    ui->calibrate->setText("Wait...");
    setEnabled(false);
    auto *window = qobject_cast<MainWindow *>(parent());
    int32_t reference = std::stol(ui->weight->text().toStdString());
    window->deviceCommand("set lc-scale " + spool_str + " 1");
    int32_t reading = (int32_t) std::stoul(window->deviceCommand("get spool-weight " + spool_str));
    string scale = std::to_string((uint32_t) (reading / reference));
    window->deviceCommand("set lc-scale " + spool_str + " " + scale);
    ui->calibrate->setText("Calibrate");
    window->updateTick();
    setEnabled(true);
}

void CalibrationDialog::onTabChange() {
    std::cout << "Tab" << ui->tabWidget->currentIndex() << "\n";
}

void CalibrationDialog::reset(uint8_t num) {
    spool_num = num;
    spool_str = std::to_string(spool_num);
    ui->tabWidget->setCurrentIndex(0);
    auto *window = qobject_cast<MainWindow *>(parent());
    uint8_t state = std::stoul( window->deviceCommand("get spool-state " + spool_str));

    if (state & 2u) ui->name->setText(window->deviceCommand("get spool-name").c_str());
    else ui->name->setText("");
}

void CalibrationDialog::onNameApply() {
    setEnabled(false);
    auto *window = qobject_cast<MainWindow *>(parent());
    window->deviceCommand("set spool-name " + spool_str + " " + ui->name->text().toStdString());
    setEnabled(true);
}

void CalibrationDialog::onRefApply() {
    setEnabled(false);
    auto *window = qobject_cast<MainWindow *>(parent());
    window->deviceCommand("set spool-ref-weight "  + spool_str + " " + std::to_string(ui->reference->value()));
    window->updateTick();
    setEnabled(true);
}
