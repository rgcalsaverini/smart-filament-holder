https://coolors.co/ffffff-3083dc-3f4045

https://doc.qt.io/qt-5/qstackedwidget.html
https://stackoverflow.com/questions/26958644/qt-loading-indicator-widget/26958738
https://doc.qt.io/qt-5/qmovie.html
http://www.ajaxload.info/#preview
https://doc.qt.io/qt-5/qtimer.html
https://stackoverflow.com/questions/36445190/c-send-udp-broadcast
https://github.com/chronoxor/CppServer
https://github.com/chriskohlhoff/asio

Instead of Asio, maybe use coroutines and simple sockets:
    https://en.cppreference.com/w/cpp/language/coroutines

Or maybe this:
    https://github.com/lewissbaker/cppcoro
    
Or maybe other libs:
    https://stackoverflow.com/questions/28027937/cross-platform-sockets