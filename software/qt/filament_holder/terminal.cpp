#include "terminal.h"
#include "ui_terminal.h"
#include "mainwindow.h"
#include <QScrollBar>

using std::string;

Terminal::Terminal(QWidget *parent)
        : QDialog(parent)
        , ui(new Ui::TerminalMsgDialog) {
    ui->setupUi(this);
    setWindowTitle("Terminal");

    connect(ui->command, SIGNAL(returnPressed()), this, SLOT(onSendClick()));
    connect(this, SIGNAL(clicked(bool)), this, SLOT(onOpen()));
    connect(ui->log, SIGNAL(clicked(bool)), this, SLOT(onOpen()));
    ui->command->setFocus();
}

Terminal::~Terminal() {
    delete ui;
}



void Terminal::onSendClick() {
    auto *window = qobject_cast<MainWindow*>(parent());
    string command = ui->command->text().toStdString();
    string response = window->deviceCommand(command);
    string log =  ui->log->text().toStdString() + "<br>" + command + "<br><strong><span style=\"color: yellow\">&nbsp;&nbsp;" + response + "</span></strong>\n";
    ui->log->setText(tr(log.c_str()));
    ui->command->clear();
    ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->maximum() + 20);
}
void Terminal::onOpen() {
    ui->command->setFocus();
}

void Terminal::showEvent(QShowEvent *e) {
    ui->command->setFocus();
    QDialog::showEvent(e);
}
