#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QCoreApplication>

#include <memory>
#include <vector>
#include "filamentholderdevice.h"
#include "spoolslot.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Terminal;

class SettingsDialog;

class CalibrationDialog;

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

    inline QSettings *getSettings() { return &app_settings; }

    std::string deviceCommand(std::string cmd) {
        usleep(10000);
        std::cout << "CMD: '" << cmd << "'";
        QCoreApplication::processEvents();
        std::string res = device.sendCommand(cmd);
        QCoreApplication::processEvents();
        std::cout << " (" << res << ")" << std::endl;
        return res;
    }

    std::string firmwareVersion();

    void updateTick();

public slots:

    void onConnectButtonClick();

    void onDisconnectButtonClick();

    void onUncConfigsButtonClick();

    void onCoConfigsButtonClick();

    void onTerminalButtonClick();

//    void onTare(uint8_t slot);
    void onOpenSpoolSettings(uint8_t slot);


private:
    void onSearchingAnimationFinished();

    void onWifiAnimationFinished();

    void onFindTimeout();

    void getSpools();

private:
    Ui::MainWindow *ui;
    FilamentHolderDevice device;
    QSettings app_settings;
    QTimer *update_timer;
    Terminal *terminal_dialog;
    SettingsDialog *settings_dialog;
    CalibrationDialog *calibration_dialog;
    std::vector<SpoolSlot *> spools;
    QThread *tick_thread;
};

#endif // MAINWINDOW_H
