#include <QCoreApplication>
#include <stdexcept>
#include "devicefinder.h"
#include "boost/asio.hpp"
#include "boost/thread.hpp"

using boost::asio::io_service;
using boost::asio::ip::udp;
using boost::asio::ip::tcp;
using boost::asio::ip::address;
using boost::asio::socket_base;
using boost::asio::buffer;
using std::string;
using std::optional;
typedef boost::shared_ptr<tcp::socket> socket_ptr;

DeviceFinder::DeviceFinder(
        uint16_t broadcast_port,
        string magic,
        uint16_t response_port
) : udp_port(broadcast_port),
    tcp_server_port(response_port),
    magic_number(magic),
    found(false),
    server_running(false) {
    getIp();
    getBroadcastIP();
}

void DeviceFinder::getIp() {
    io_service io;
    udp::socket socket(io);
    udp::endpoint remote_endpoint;
    socket.open(udp::v4());
    remote_endpoint = udp::endpoint(address::from_string("8.8.8.8"), 80);
    socket.connect(remote_endpoint);
    own_ip = socket.local_endpoint().address().to_string();
}

void DeviceFinder::getBroadcastIP() {
    broadcast_ip = string(own_ip);
    broadcast_ip = broadcast_ip.substr(0, broadcast_ip.find_last_of('.')) + ".255";
}

void DeviceFinder::sendBroadcast() {
    io_service io_service;
    udp::socket socket(io_service);
    udp::endpoint remote_endpoint;

    if (broadcast_payload.empty()) generateBroadcastPayload();

    socket.open(udp::v4());

    socket.set_option(udp::socket::reuse_address(true));
    socket.set_option(socket_base::broadcast(true));

    remote_endpoint = udp::endpoint(address::from_string(broadcast_ip), udp_port);

    boost::system::error_code err;
    socket.send_to(buffer(broadcast_payload, broadcast_payload.length()), remote_endpoint, 0, err);

    socket.close();
}

void DeviceFinder::generateBroadcastPayload() {
    string port_str = std::to_string(tcp_server_port);
    broadcast_payload = string(magic_number) \
 + string({(char) own_ip.length()}) \
 + string({(char) port_str.length()}) \
 + own_ip \
 + port_str;
}

std::pair<std::string, uint16_t> DeviceFinder::find(uint16_t max_tries) {
    boost::thread broadcast_response_server(boost::bind(&DeviceFinder::responseServer, this));

    // Wait for the server to start up
    while (!server_running) {
        QCoreApplication::processEvents();
        usleep(20000);
    }

    for (uint16_t i = 0; !found; i++) {
        sendBroadcast();
        for (uint16_t j = 0; j < 25 && !found; j++) {
            QCoreApplication::processEvents();
            usleep(40000);
        }

        if (i > max_tries && max_tries != 0) throw std::runtime_error("timeout");
    }

    return std::make_pair(target_host, target_port);
}

void DeviceFinder::responseServer() {
    while (DeviceFinder::portInUse(tcp_server_port)) tcp_server_port++;
    io_service service;
    tcp::endpoint ep(tcp::v4(), tcp_server_port);
    tcp::acceptor acc(service, ep);
    socket_ptr socket(new tcp::socket(service));
    server_running = true;

    while (true) {
        acc.accept(*socket);
        bool magic_match;
        char data[64];
        size_t len = socket->read_some(buffer(data));
        if (len > 8) {
            uint16_t k = 0;
            magic_match = true;
            for (const auto &ch : magic_number) {
                if (ch != data[k]) {
                    magic_match = false;
                    break;
                };
                k++;
            }
            if (!magic_match) continue;
            uint16_t remote_port = std::stoi(data + 8);

            std::cout << "Found " << socket->remote_endpoint().address().to_string() << ":" << remote_port << "\n";
            target_host = socket->remote_endpoint().address().to_string();
            target_port = remote_port;
            found = true;
            break;
        }
    }

    socket->close();
}

bool DeviceFinder::portInUse(uint16_t port) {
    io_service svc;
    tcp::acceptor a(svc);

    boost::system::error_code ec;
    a.open(tcp::v4(), ec) || a.bind({tcp::v4(), port}, ec);

    return ec == boost::asio::error::address_in_use;
}
