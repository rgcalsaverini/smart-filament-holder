#ifndef CPPUDP_FILAMENTHOLDERDEVICE_H
#define CPPUDP_FILAMENTHOLDERDEVICE_H

#include "boost/asio.hpp"
#include <iostream>
#include <string>
#include <optional>
#include <vector>

class FilamentHolderDevice {
public:
    explicit FilamentHolderDevice(std::string host = "", uint16_t port = 0);

    ~FilamentHolderDevice();

    void connect();

    void disconnect();

    std::string sendCommand(const std::string &cmd);

    std::pair<int32_t, std::optional<int32_t>> getSpoolWeight(uint8_t spool_id);

    static std::vector<int32_t> splitWeightFields(const std::string& response);

    void setHostAndPort(std::string host, uint16_t port);

    inline std::string getHost() { return hostname ;}

    inline uint16_t getPort() { return port ;}

private:


private:
    std::optional<boost::asio::ip::tcp::socket> socket;
    std::string hostname;
    uint16_t port;
    boost::asio::io_service io_context;
};


#endif //CPPUDP_FILAMENTHOLDERDEVICE_H
