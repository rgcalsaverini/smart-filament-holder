#ifndef FILAMENT_HOLDER_TERMINAL_H
#define FILAMENT_HOLDER_TERMINAL_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class TerminalMsgDialog; }
QT_END_NAMESPACE

class Terminal : public QDialog {
Q_OBJECT

public:
    Terminal(QWidget *parent = nullptr);

    ~Terminal();

public slots:
    void onSendClick();
    void onOpen();

protected:
    void showEvent(QShowEvent *e);

private:


private:
    Ui::TerminalMsgDialog *ui;
};

#endif //FILAMENT_HOLDER_TERMINAL_H
