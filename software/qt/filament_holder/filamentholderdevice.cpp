#include "filamentholderdevice.h"

#include <utility>
#include "boost/thread.hpp"

using std::string;
using std::pair;
using std::vector;
using std::optional;
using boost::asio::io_service;
using boost::asio::ip::tcp;
using boost::asio::ip::address;

FilamentHolderDevice::FilamentHolderDevice(string host, uint16_t port) : hostname(move(host)),
                                                                         port(port) {
}

FilamentHolderDevice::~FilamentHolderDevice() {
    if (socket) socket->close();
}

void FilamentHolderDevice::connect() {
    socket = tcp::socket(io_context);
    tcp::endpoint endpoint(address::from_string(hostname), port);
    boost::system::error_code ec;
    socket->connect(endpoint, ec);
}

void FilamentHolderDevice::disconnect() {
    if (socket && socket->is_open()) {
        socket->close();
    }
    socket = std::nullopt;
}


std::string FilamentHolderDevice::sendCommand(const std::string &cmd) {
    if (!socket) connect();
    socket->write_some(boost::asio::buffer(cmd, cmd.length()));
    char response[64];
    size_t len = socket->read_some(boost::asio::buffer(response, 64));
    return string(response, len);
}

void FilamentHolderDevice::setHostAndPort(std::string new_host, uint16_t new_port) {
    hostname = std::move(new_host);
    port = new_port;
}

pair<int32_t, optional<int32_t>> FilamentHolderDevice::getSpoolWeight(uint8_t spool_id) {
    string res = sendCommand("get spool-ref-weight " + std::to_string(spool_id)); // std::stol();
    // Reference not set
    if (res.find(' ') == std::string::npos) {
        return {std::stol(res), std::nullopt};
    }
    vector<int32_t> weights = FilamentHolderDevice::splitWeightFields(res);
    int32_t reduction = weights[0] - weights[2];
    return {weights[2], weights[1] - reduction};
}

std::vector<int32_t> FilamentHolderDevice::splitWeightFields(const std::string &str) {
    string part_2 = str.substr(str.find(' ') + 1);

    int32_t initial = std::stol(str.substr(0, str.find(' ')));
    int32_t reference = std::stol(part_2.substr(0, part_2.find(' ')));
    int32_t current = std::stol(part_2.substr(part_2.find(' ')));

    return {initial, reference, current};
}

