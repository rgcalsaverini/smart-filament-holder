#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>
#include <QDialog>
#include <QTimer>
#include <QThread>
#include <string>
#include <stdexcept>
#include <iomanip>
#include <sstream>

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "devicefinder.h"
#include "filamentholderdevice.h"
#include "settingsdialog.h"
#include "terminal.h"
#include "calibration.h"

using std::optional;
using std::string;
using std::vector;
using std::pair;

// TODO: tick on thread: https://doc.qt.io/qt-5/qthread.html

MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent), ui(new Ui::MainWindow), app_settings("rgcalsaverini", "filament_scale") {
    ui->setupUi(this);

    centralWidget()->layout()->setContentsMargins(0, 0, 0, 0);
    ui->topBar->setCurrentIndex(0);
    ui->center->setCurrentIndex(0);

    tick_thread = new QThread(this);
    tick_thread->start();
//    while(!tick_thread->isRunning()) usleep(100000);
    update_timer = new QTimer(nullptr);
//    update_timer->moveToThread(tick_thread);

    connect(update_timer, &QTimer::timeout, this, &MainWindow::updateTick);
    connect(ui->connectButton, SIGNAL(clicked(bool)), this, SLOT(onConnectButtonClick()));
    connect(ui->disconnectButton, SIGNAL(clicked(bool)), this, SLOT(onDisconnectButtonClick()));
    connect(ui->settingsButton, SIGNAL(clicked(bool)), this, SLOT(onUncConfigsButtonClick()));
    connect(ui->settingsButtonConnected, SIGNAL(clicked(bool)), this, SLOT(onCoConfigsButtonClick()));
    connect(ui->terminalButton, SIGNAL(clicked(bool)), this, SLOT(onTerminalButtonClick()));

    if (app_settings.value("auto_connect", true).toBool()) onConnectButtonClick();

    terminal_dialog = new Terminal(this);
    settings_dialog = new SettingsDialog(this);
    calibration_dialog = new CalibrationDialog(this);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::updateTick() {
    std::cout << "tick\n";

    for (auto &spool: spools) {
        QCoreApplication::processEvents();
        usleep(10000);
        pair<int32_t, optional<int32_t>> weight_info = device.getSpoolWeight(spool->getNum());
        double weight_kg = weight_info.first / 1000.0;
        optional<int32_t> left_g = weight_info.second;
        std::stringstream stream;

        string weight_value;
        string left_value = "--";
        if (app_settings.value("metric", true).toBool()) {
            stream << std::fixed << std::setprecision(2) << weight_kg;
            weight_value = stream.str() + " kg";
            if (left_g) left_value = std::to_string(*left_g) + " g";
        } else {
            weight_kg *= 2.20462;
            stream << std::fixed << std::setprecision(2) << weight_kg;
            weight_value = stream.str() + " lbs";

            if (left_g) {
                *left_g *= 0.035274;
                left_value = std::to_string(*left_g) + " oz";
            }

        }
        spool->updateWeight(weight_value);
        spool->updateFilamentLeft(left_value);
    }
}

void MainWindow::onConnectButtonClick() {
    ui->connectButton->setEnabled(false);
    ui->connectButton->setText("Find");
    ui->notConnectedLabel->setText("Searching for devices on the network...");

    QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(this);
    opacityEffect->setOpacity(1.0);
    ui->unconnectedImage->setGraphicsEffect(opacityEffect);
    QPropertyAnimation *anim = new QPropertyAnimation(this);
    anim->setTargetObject(opacityEffect);
    anim->setPropertyName("opacity");
    anim->setDuration(300);
    anim->setStartValue(opacityEffect->opacity());
    anim->setEndValue(0);
    anim->setEasingCurve(QEasingCurve::OutQuad);
    anim->start(QAbstractAnimation::KeepWhenStopped);
    QObject::connect(anim, &QAbstractAnimation::finished, this, &MainWindow::onSearchingAnimationFinished);

}

void MainWindow::onDisconnectButtonClick() {
    std::cout << "START\n";
    update_timer->stop();
    usleep(500000);
    QPropertyAnimation *anim1 = new QPropertyAnimation(ui->topBar, "maximumHeight");
    std::cout << "disconnect\n";
    device.disconnect();
    anim1->setDuration(300);
    anim1->setStartValue(60);
    anim1->setEndValue(100);
    anim1->start();
    ui->topBar->setCurrentIndex(0);
    ui->center->setCurrentIndex(0);
    ui->unconnectedImage->setPixmap(QPixmap("/home/rui/projects/qt_test/filament_holder/res/link_broken.png"));
    ui->connectButton->setEnabled(true);
    ui->notConnectedLabel->setText("No devices connected");
}

void MainWindow::onSearchingAnimationFinished() {
    ui->unconnectedImage->setPixmap(QPixmap("/home/rui/projects/qt_test/filament_holder/res/wifi.png"));
    QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect(this);
    opacityEffect->setOpacity(0.0);
    ui->unconnectedImage->setGraphicsEffect(opacityEffect);
    QPropertyAnimation *anim = new QPropertyAnimation(this);
    anim->setTargetObject(opacityEffect);
    anim->setPropertyName("opacity");
    anim->setDuration(300);
    anim->setStartValue(opacityEffect->opacity());
    anim->setEndValue(1.0);
    anim->setEasingCurve(QEasingCurve::OutQuad);
    anim->start(QAbstractAnimation::KeepWhenStopped);

    QObject::connect(anim, &QAbstractAnimation::finished, this, &MainWindow::onWifiAnimationFinished);
}

void MainWindow::onWifiAnimationFinished() {
    string magic = "\x19\xcc\xaa\x84\x8d\x63\xa8\xac";

    try {
        DeviceFinder finder(5001, magic);
        auto target = finder.find(5);
        device.setHostAndPort(target.first, target.second);
        getSpools();
        updateTick();
        update_timer->start(app_settings.value("refresh_interval", 2).toInt() * 1000);
    } catch (std::runtime_error &e) {
        if (string(e.what()) == "timeout") onFindTimeout();
        return;
    }

    QPropertyAnimation *anim2 = new QPropertyAnimation(ui->topBar, "maximumHeight");
    anim2->setDuration(300);
    anim2->setStartValue(100);
    anim2->setEndValue(60);
    anim2->start();

    ui->topBar->setCurrentIndex(1);
    ui->center->setCurrentIndex(1);
    string hostname = device.getHost() + ":" + std::to_string(device.getPort());
    ui->ipLabel->setText(hostname.c_str());
}

void MainWindow::onFindTimeout() {
    ui->notConnectedLabel->setText("Could not find any devices");
    ui->connectButton->setText("Try Again");
    ui->connectButton->setEnabled(true);
    ui->unconnectedImage->setPixmap(QPixmap("/home/rui/projects/qt_test/filament_holder/res/link_broken_r.png"));
}

void MainWindow::onUncConfigsButtonClick() {
    SettingsDialog dialog(this);
    dialog.exec();
}

void MainWindow::onCoConfigsButtonClick() {
    settings_dialog->enableDeviceSettings(true);
    settings_dialog->exec();
}

void MainWindow::onTerminalButtonClick() {
    uint16_t original_auto_update = app_settings.value("refresh_interval", 2).toInt();
    update_timer->stop();
    terminal_dialog->exec();
    update_timer->start(app_settings.value("refresh_interval", 2).toInt() * 1000);
}

void MainWindow::onOpenSpoolSettings(uint8_t slot) {
    uint16_t original_auto_update = app_settings.value("refresh_interval", 2).toInt();
    update_timer->stop();
    usleep(100000);
    calibration_dialog->reset(slot);
    calibration_dialog->exec();
    getSpools();
    update_timer->start(app_settings.value("refresh_interval", 2).toInt() * 1000);
}

void MainWindow::getSpools() {
    string response = deviceCommand("get spools");
    uint8_t num_spools = response[0] - '0';


    if (spools.size() != num_spools) {
        if (!spools.empty()) {
            for (auto &spool : spools) delete spool;
            spools.clear();
        }
        spools.resize(num_spools);

        for (uint32_t i = 0; i < spools.size(); i++) {
            spools[i] = new SpoolSlot(this, i);
            spools[i]->setEnabled(response[i + 1] == '1');
            ui->spoolContainer->layout()->addWidget(spools[i]);
            connect(spools[i], SIGNAL(settingsClicked(uint8_t)), this, SLOT(onOpenSpoolSettings(uint8_t)));
        }
    }
    uint8_t spool_state;

    for (uint32_t i = 0; i < spools.size(); i++) {
        if (response[i + 1] == '1') {
            spools[i]->setEnabled(true);
            spool_state = std::stoul(deviceCommand("get spool-state " + std::to_string(i)));
        } else {
            spools[i]->setEnabled(false);
            spool_state = 0;
        }
        if (spool_state & 2u) {
            string name = deviceCommand("get spool-name " + std::to_string(i));
            spools[i]->updateName(name);
        } else {
            spools[i]->updateName("Spool " + std::to_string(i + 1));
        }
    }
}

std::string MainWindow::firmwareVersion() {
    return deviceCommand("get version");
}
