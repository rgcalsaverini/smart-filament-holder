#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>
#include <QDialog>
#include <string>
#include <stdexcept>
#include <iostream>

#include "settingsdialog.h"
#include "./ui_settingsdialog.h"

using std::string;

SettingsDialog::SettingsDialog(QWidget *parent)
        : QDialog(parent)
        , device_settings_enabled(false)
        , ui(new Ui::Dialog) {
    ui->setupUi(this);
    ui->udpPortInput->setValidator(new QIntValidator(1000, 65000, this));

    connect(ui->actionsBox->button(QDialogButtonBox::Apply), SIGNAL(clicked(bool)), this, SLOT(onSaveClick()));
    connect(ui->actionsBox->button(QDialogButtonBox::Discard), SIGNAL(clicked(bool)), this, SLOT(onDiscardClick()));
    connect(ui->actionsBox->button(QDialogButtonBox::RestoreDefaults), SIGNAL(clicked(bool)), this, SLOT(onRestoreClick()));
    setWindowTitle("Settings");
    enableDeviceSettings(false);

    reload();
}

SettingsDialog::~SettingsDialog() {
    delete ui;
}

void SettingsDialog::reload() {
    auto *window = qobject_cast<MainWindow*>(parent());
    QSettings *settings = window->getSettings();

    ui->autoConnect->setCheckState(settings->value("auto_connect", true).toBool() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    ui->refreshInterval->setValue(settings->value("refresh_interval", 2).toInt());
    ui->unitsSelect->setCurrentIndex(settings->value("metric", true).toBool() ? 0 : 1);

    if (device_settings_enabled) {
        ui->firmware->setText(window->firmwareVersion().c_str());
    }
}

void SettingsDialog::onSaveClick() {
    auto *window = qobject_cast<MainWindow*>(parent());
    QSettings *settings = window->getSettings();

    settings->setValue("auto_connect", ui->autoConnect->isChecked());
    settings->setValue("refresh_interval", ui->refreshInterval->value());
    settings->setValue("metric", ui->unitsSelect->currentIndex() == 0);

    // Force dumping to disk
    settings->sync();

    close();
}

void SettingsDialog::onDiscardClick(){
    close();
}

void SettingsDialog::onRestoreClick(){
    ui->udpPortInput->setText("5001");
    ui->maxRetries->setValue(10);
    ui->autoConnect->setCheckState(Qt::CheckState::Checked);
    ui->refreshInterval->setValue(2);
    ui->unitsSelect->setCurrentIndex(0);

    if(device_settings_enabled) {
        ui->samples->setValue(3);
        ui->commPort->setText("5001");
    }
}

void SettingsDialog::enableDeviceSettings(bool value) {
    device_settings_enabled = value;

    if(device_settings_enabled) {
        ui->deviceOnlySettings->setEnabled(true);
    } else {
        ui->deviceOnlySettings->setEnabled(false);
    }

    reload();
}
