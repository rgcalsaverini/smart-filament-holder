#ifndef SPOOLSLOT_H
#define SPOOLSLOT_H

#include <QWidget>


QT_BEGIN_NAMESPACE
namespace Ui { class SpoolSlot; }
QT_END_NAMESPACE

class SpoolSlot : public QWidget {
Q_OBJECT

public:
    explicit SpoolSlot(QWidget *parent, uint8_t num);

    ~SpoolSlot() override;

    void updateWeight(std::string value);

    void updateName(std::string name);

    void updateFilamentLeft(std::string value);

    inline uint8_t getNum() const { return slot_number; }

private slots:

    // void onTare() { emit tareClicked(slot_number); }

    void onSettingsClick() { emit settingsClicked(slot_number); }

signals:

    // void tareClicked(uint8_t slot);

    void settingsClicked(uint8_t slot);

private:
    Ui::SpoolSlot *ui;
    uint8_t slot_number;
};

#endif // SPOOLSLOT_H
