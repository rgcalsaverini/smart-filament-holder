#include <string.h>
#include "app.h"

#include "mcc_generated_files/drivers/uart.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/interrupt_manager.h"
#include "mcc_generated_files/memory.h"

#include "tcp_api.h"
#include "web.h"
#include "wifi_credentials.h"

#include "external/pic8-libs/esp8266/esp8266.h"
#include "external/pic8-libs/esp8266/iot.h"
#include "external/pic8-libs/esp8266/ap.h"
#include "external/pic8-libs/hx711/hx711.h"
#include "external/pic8-libs/eeprom/eeprom.h"
#include "mcc_generated_files/tmr0.h"

const char app_firmware_version[] = "v0.1-beta0";
const char app_magic_num[8] = {25, 204, 170, 132, 141, 99, 168, 172};
const char app_port[] = "5000";
const char app_broadcast_port[] = "5001";
const uint8_t app_link_broadcast_listen = 0;
const uint8_t app_link_broadcast_respond = 1;
const uint8_t app_link_comm = 2;
uint8_t active_spool = 0;
bool pooling_active = 0;

void APP_initialize(void) {
    EEPROM_initialize(DATAEE_ReadByte, DATAEE_WriteByte);
    HX711_initialize(APP_delayUS, APP_disableInterrupts, APP_enableInterrupts, APP_hx711Read, APP_hx711Write);
    ESP8266_initialize(APP_UART0Read, APP_UART0Write, APP_switchESP);
    ESP8266_enable();
}

void APP_startDevice() {
    ESP8266_startIOTServer(app_broadcast_port,
                           app_port,
                           app_link_broadcast_listen,
                           app_link_broadcast_respond,
                           app_link_comm,
                           APP_replyToBroadcast,
                           APP_tcpEndpoint);
    ESP8266_connect(app_ap_ssid, app_ap_pwd, false);
    if (!APP_eepromInitialized()) APP_initializeEeprom();
    HX711_setAverageTimes(EEPROM_readU8(ADDR_SAMPLES));
    APP_activateSpool(0);
    TMR0_InterruptHandler = APP_timer0;
}

void APP_startConfigServerAP() {
    // Setup the ESP configs
    ESP8266_setMode(ESP8266_SOFTAP_AND_STATION);
    ESP8266_setMultipleConnections(true);
    ESP8266_setAutoConnect(false);
    ESP8266_setEcho(true);

    // Create server
    ESP8266_createAP("TEST", "cavalodado123", "192.168.1.1", 5, 1, false, false);
    ESP8266_startServer("80", 1);

    char method[12];
    char path[100];
    uint8_t link;

    while(link != 99) {
        ESP8266_getRequest(method, path, &link);
//        if (_APP_strcmp())
        ESP8266_sendHttpResponse("192.168.1.1", "200 OK", web_settings, link);
    }
}


void APP_UART0Write(char ch) {
    uart[UART0].Write(ch);
}

char APP_UART0Read() {
    return uart[UART0].Read();
}

void APP_switchESP(bool state) {
    LATAbits.LATA4 = state;
}

void APP_replyToBroadcast(char *data, uint16_t len) {
    if (len < 19) return;
    for (uint8_t i = 0; i < 8; i++) {
        if (data[i] != app_magic_num[i]) return;
    }
    uint8_t len_ip = data[8];
    uint8_t len_port = data[9];
    uint8_t start_ip = 10;
    uint8_t start_port = start_ip + len_ip;

    char host[200];
    char port[8];

    memcpy(host, data + start_ip, len_ip);
    host[len_ip] = 0;
    memcpy(port, data + start_port, len_port);
    port[len_port] = 0;

    char payload[12];

    memcpy(payload, app_magic_num, 8);
    memcpy(payload + 8, app_port, 4);

    ESP8266_sendDataOnceSafe(ESP8266_TCP, host, port, app_link_broadcast_respond, payload, 12, false);
}

bool _APP_strcmp(char *str1, const char *str2) {
    uint8_t i = 0;
    while(true) {
        if (str1[i] != str2[i]) return false;
        if (str1[i] == 0 || str2[i] == 0) break;
        i++;
    }
    return true;
}

uint16_t APP_tcpEndpoint(char *data, uint16_t len, char **response) {
    tcp_endpoint endpoint;
    tcp_endpoint_method method;

    API_parseRequest(&endpoint, &method, data, len);

    if (endpoint == EP_INVALID_ARG) {
        if (api_uint_args[0] == (uint8_t) ERR_STR_MIN) return _APP_setMessage("str-too-short");
        if (api_uint_args[0] == (uint8_t) ERR_INT_MIN) return _APP_setMessage("int-too-small");
        if (api_uint_args[0] == (uint8_t) ERR_INT_MAX) return _APP_setMessage("int-too-big");
        if (api_uint_args[0] == (uint8_t) ERR_INT_CHAR) {
            char msg[] = "invalid-ch-on-int X";
            msg[18] = api_str_arg[0];
            return _APP_setMessage(msg);
        }
        if (api_uint_args[0] == (uint8_t) ERR_INT_MISSING) return _APP_setMessage("missing-int");
        if (api_uint_args[0] == (uint8_t) ERR_TRAILING) return _APP_setMessage("trailing-ch");
        return _APP_setMessage("arg-error");
    }
    if (endpoint == EP_NON_EXISTENT) return _APP_setMessage("unknown-cmd");
    if (endpoint == EP_ECHO_INT) return _APP_itoa(api_uint_args[0]);
    if (endpoint == EP_RESET) {
        __asm__ volatile ("reset");
        return 0;
    }
    if (endpoint == EP_CONVERT) {
        APP_activateSpool(api_uint_args[0]);
        return _APP_itoa(HX711_convert(api_uint_args[1]));
    }

    if (method == EP_GET) {
        /*************** VERSION ***************/
        if (endpoint == EP_VERSION) {
            return _APP_setMessage(app_firmware_version);
        }
            /*************** SPOOLS ***************/
        else if (endpoint == EP_SPOOLS) {
            char spools[API_MAX_SPOOLS + 2];
            spools[API_MAX_SPOOLS + 1] = 0;
            uint8_t connected = APP_getSpoolsConnected();
            spools[0] = API_MAX_SPOOLS + '0';
            for (uint8_t i = 0; i < API_MAX_SPOOLS; i++) {
                spools[i + 1] = (connected >> i) & 1 ? '1' : '0';
            }
            return _APP_setMessage(spools);
        }
            /*************** SPOOL-STATE ***************/
        else if (endpoint == EP_SPOOL_STATE) {
            return _APP_itoa(DATAEE_ReadByte(SP_ADDR(api_uint_args[0], ADDR_STATE)));
        }
            /*************** SPOOL-NAME ***************/
        else if (endpoint == EP_SPOOL_NAME) {
//            if (!APP_isSpoolFlagSet(api_uint_args[0], APP_SPOOL_HAS_NAME)) {
//                return _APP_setMessage(" ");
//            }
            return EEPROM_readStrBE(SP_ADDR(api_uint_args[0], ADDR_NAME), _esp8266_endpointResponse);
        }
            /*************** SPOOL-WEIGHT ***************/
        else if (endpoint == EP_SPOOL_WEIGHT) {
            APP_activateSpool(api_uint_args[0]);
            return _APP_itoa(HX711_readComplete());
        }
            /*************** SPOOL-REF_WEIGHT ***************/
        else if (endpoint == EP_SPOOL_REF_WEIGHT) {
            /*
             If the APP_SPOOL_HAS_REF_W flag is set, will return three integers, in order:
             initial weight, reference weight, current weight.
             If the flag is not set, will return only current weight
            */
            APP_activateSpool(api_uint_args[0]);
            if (!APP_isSpoolFlagSet(active_spool, APP_SPOOL_HAS_REF_W)) {
                return _APP_itoa(HX711_readComplete());
            }
            uint32_t initial_weight = HX711_convert(EEPROM_readU32BE(SP_ADDR(active_spool, ADDR_INITIAL_W)));
            uint32_t reference_weight = EEPROM_readU32BE(SP_ADDR(active_spool, ADDR_REF_W));
            uint8_t final_len = _APP_itoa(initial_weight);
            _esp8266_endpointResponse[final_len] = ' ';
            final_len++;
            final_len += _APP_itoaOffset(reference_weight, final_len);
            _esp8266_endpointResponse[final_len] = ' ';
            final_len++;
            final_len += _APP_itoaOffset(HX711_readComplete(), final_len);
            return final_len;
        }
            /*************** LC-VALUE ***************/
        else if (endpoint == EP_LC_VALUE) {
            APP_activateSpool(api_uint_args[0]);
            uint32_t value = HX711_read();
            if (value & 0x800000u) value = ((uint32_t)(0xFF) << 24u) | value;
            return _APP_itoa(value);
        }
            /*************** LC-VALUE-AVERAGE ***************/
        else if (endpoint == EP_LC_VALUE_AVERAGE) {
            APP_activateSpool(api_uint_args[0]);
            return _APP_itoa(HX711_readAverage(api_uint_args[1], api_uint_args[2]));
        }
            /*************** LC-SAMPLES ***************/
        else if (endpoint == EP_LC_SAMPLES) {
            return _APP_itoa(_hx711_average_times);
        }
            /*************** LC-SCALE ***************/
        else if (endpoint == EP_LC_SCALE) {
            return _APP_itoa(EEPROM_readU32BE(SP_ADDR(api_uint_args[0], ADDR_SCALE)));
        }
            /*************** LC-OFFSET ***************/
        else if (endpoint == EP_LC_OFFSET) {
            return _APP_itoa(EEPROM_readU32BE(SP_ADDR(api_uint_args[0], ADDR_OFFSET)));
        }
            /*************** EEPROM ***************/
        else if (endpoint == EP_EEPROM) {
            uint8_t data[128];
            EEPROM_readNBE(api_uint_args[0], data, api_uint_args[1]);
            uint8_t len = 0;
            for (uint8_t i = 0; i < api_uint_args[1]; i++) {
                len += _APP_itoaOffset(data[i], len);
                _esp8266_endpointResponse[len] = ' ';
                len++;
            }
            return len;
        }
    } else {
        /*************** SPOOL-NAME ***************/
        if (endpoint == EP_SPOOL_NAME) {
            EEPROM_writeStrBE(SP_ADDR(api_uint_args[0], ADDR_NAME), api_str_arg, api_uint_args[1]);
            APP_setSpoolFlag(api_uint_args[0], APP_SPOOL_HAS_NAME, true);
            return _APP_setMessage("ok");
        }
        /*************** SPOOL-REF-WEIGHT ***************/
        if (endpoint == EP_SPOOL_REF_WEIGHT) {
            APP_activateSpool(api_uint_args[0]);
            uint32_t initial_weight = HX711_readAverage(10, 0);
            EEPROM_writeU32BE(SP_ADDR(active_spool, ADDR_REF_W), api_uint_args[1]);
            EEPROM_writeU32BE(SP_ADDR(active_spool, ADDR_INITIAL_W), initial_weight);
            APP_setSpoolFlag(api_uint_args[0], APP_SPOOL_HAS_REF_W, true);
            return _APP_setMessage("ok");
        }
            /*************** LC-SAMPLES ***************/
        else if (endpoint == EP_LC_SAMPLES) {
            HX711_setAverageTimes(api_uint_args[0]);
            EEPROM_writeU8(ADDR_SAMPLES, api_uint_args[1]);
            return _APP_setMessage("ok");
        }
            /*************** LC-OFFSET ***************/
        else if (endpoint == EP_LC_OFFSET) {
            EEPROM_writeU32BE(SP_ADDR(api_uint_args[0], ADDR_OFFSET), api_uint_args[1]);
            return _APP_setMessage("ok");
        }
            /*************** LC-SCALE ***************/
        else if (endpoint == EP_LC_SCALE) {
            EEPROM_writeU32BE(SP_ADDR(api_uint_args[0], ADDR_SCALE), api_uint_args[1]);
            return _APP_setMessage("ok");
        }
            /*************** EEPROM ***************/
        else if (endpoint == EP_EEPROM) {
            EEPROM_writeU8(api_uint_args[0], api_uint_args[1]);
            return _APP_itoa(api_uint_args[1]);
        }
    }

    return _APP_setMessage("unexpected-error");

//    if (_APP_substrMatch(data, "get spool-state ", 0)) {
//        uint32_t spool;
//        bool success = _APP_atoi(data, 16, len, &spool);
//        if (!success) return _APP_setMessage("invalid-int");
//        if(spool > 5) return _APP_setMessage("invalid-value");
//        return _APP_itoa(DATAEE_ReadByte(0x80 + 0x80 * spool + 0x10));
//    }
//    if (_APP_substrMatch(data, "convert ", 0)) {
//        uint32_t value;
//        bool success = _APP_atoi(data, 9, len, &value);
//        if (!success) return _APP_setMessage("invalid-int");
//        return _APP_itoa(HX711_convert(value));
//    }
//    if (_APP_substrMatch(data, "set lc-samples ", 0)) {
//        uint8_t value;
//        bool success = _APP_atoi(data, 15, len, &value);
//        if (!success) return _APP_setMessage("invalid-int");
//        if (value < 1 || value > 20) return _APP_setMessage("invalid-value");
//        HX711_setAverageTimes(value);
//        return _APP_setMessage("ok");
//    }
//    if (_APP_substrMatch(data, "set lc-offset ", 0)) {
//        uint32_t value;
//        bool success = _APP_atoi(data, 14, len, &value);
//        if (!success) return _APP_setMessage("invalid-int");
//        HX711_setOffset(value);
//        EEPROM_writeU32BE(0x80, value);
//        return _APP_setMessage("ok");
//    }
//    if (_APP_substrMatch(data, "set lc-scale ", 0)) {
//        uint32_t value;
//        bool success = _APP_atoi(data, 13, len, &value);
//        if (!success) return _APP_setMessage("invalid-int");
//        HX711_setScale(value);
//        EEPROM_writeU32BE(0x84, value);
//        return _APP_setMessage("ok");
//    }
//    if (_APP_substrMatch(data, "set spool-name ", 0)) {
//        if (len < 16 || len > 100) return _APP_setMessage("invalid-value");
//        EEPROM_writeStrBE(0x91, data + 15, len - 15);
//        APP_setSpoolFlag(0, APP_SPOOL_HAS_NAME, true);
//        return _APP_setMessage("ok");
//    }
//    if (_APP_substrMatch(data, "set spool-ref-weight ", 0)) { // 21
//        uint32_t initial_weight = HX711_readAverage(3, 0);
//        uint32_t reference_weight;
//        bool success = _APP_atoi(data, 21, len, &reference_weight);
//        if (!success) return _APP_setMessage("invalid-int");
//        EEPROM_writeU32BE(0x8c, reference_weight);
//        EEPROM_writeU32BE(0x88, initial_weight);
//        APP_setSpoolFlag(0, APP_SPOOL_HAS_REF_W, true);
//        return _APP_setMessage("ok");
//    }
//    if (_APP_substrMatch(data, "get spool-ref-weight", 0) && len == 20) {
//        /*
//         If the APP_SPOOL_HAS_REF_W flag is set, will return three integers, in order:
//         initial weight, reference weight, current weight.
//         If the flag is not set, will return only current weight
//        */
//        if(!APP_isSpoolFlagSet(0, APP_SPOOL_HAS_REF_W)) {
//            return _APP_itoa(HX711_readComplete());
//        }
//        uint32_t initial_weight = HX711_convert(EEPROM_readU32BE(0x88));
//        uint32_t reference_weight = EEPROM_readU32BE(0x8c);
//        uint8_t final_len = _APP_itoa(initial_weight);
//        _esp8266_endpointResponse[final_len] = ' ';
//        final_len++;
//        final_len += _APP_itoaOffset(reference_weight, final_len);
//        _esp8266_endpointResponse[final_len] = ' ';
//        final_len++;
//        final_len += _APP_itoaOffset(HX711_readComplete(), final_len);
//        return final_len;
//    }
//    if (_APP_substrMatch(data, "get spools", 0) && len == 10) {
//        char spools[] = "40000";
//        for(uint8_t i = 0 ; i < 4 ; i++) {
//            spools[i+1] = APP_isSpoolFlagSet(i, APP_SPOOL_EN) ? '1' : '0';
//        }
//        return _APP_setMessage(spools);
//    }
//    if (_APP_substrMatch(data, "get spool-name", 0) && len == 14) {
//        if(!APP_isSpoolFlagSet(0, APP_SPOOL_HAS_NAME)) {
//            return _APP_setMessage(" ");
//        }
//        return EEPROM_readStrBE(0x91, _esp8266_endpointResponse);
//    }
//    if (_APP_substrMatch(data, "get lc-samples", 0) && len == 14) {
//        return _APP_itoa(_hx711_average_times);
//    }
//    if (_APP_substrMatch(data, "get lc-offset", 0) && len == 13) {
//        return _APP_itoa(_hx711_offset);
//    }
//    if (_APP_substrMatch(data, "get lc-scale", 0) && len == 12) {
//        return _APP_itoa(_hx711_scale);
//    }
//    if (_APP_substrMatch(data, "get lc-value", 0) && len == 12) {
//        uint32_t value = HX711_read();
//        if (value & 0x800000u) value = ((uint32_t) (0xFF) << 24u) | value;
//        return _APP_itoa(value);
//    }
//    if (_APP_substrMatch(data, "get lc-value-average", 0) && len == 20) {
//        return _APP_itoa(HX711_readAverage(_hx711_average_times, _hx711_interval_ms * 1000));
//    }
//    if (_APP_substrMatch(data, "get spool-weight", 0) && len == 16) {
//        return _APP_itoa(HX711_readComplete());
//    }
//    if (_APP_substrMatch(data, "get version", 0) && len == 11) {
//        return _APP_setMessage(app_firmware_version);
//    }
//    if (_APP_substrMatch(data, "check", 0) && len == 5) {
//        return _APP_setMessage("alive");
//    }
//    return _APP_setMessage("unknown-cmd");
}

uint8_t _APP_itoa(uint32_t value) {
    return _APP_itoaOffset(value, 0);
}

uint8_t _APP_itoaOffset(uint32_t value, uint16_t offset) {
    uint8_t len = 1;
    uint32_t value_copy = value;
    while (value_copy > 9) {
        len++;
        value_copy /= 10;
    }
    for (uint8_t i = len; i != 0; i--) {
        _esp8266_endpointResponse[i - 1 + offset] = value % 10 + '0';
        value /= 10;
    }
    return len;
}


bool _APP_atoi(char *data, uint8_t start, uint8_t len, uint32_t *value) {
    *value = 0;
    for (uint8_t i = start; i < len; i++) {
        if (data[i] < '0' || data[i] > '9') return false;
        *value = (*value) * 10 + (data[i] - '0');
    }
    return true;
}

uint8_t _APP_setMessage(char *message) {
    uint8_t len = strlen(message);
    memcpy(_esp8266_endpointResponse, message, len);
    return len;
}

void APP_delayUS(uint16_t delay) {
    while (delay > 0) {
        __delay_us(1);
        delay--;
    }
}

void APP_disableInterrupts() {
    INTERRUPT_GlobalInterruptDisable();
    INTERRUPT_PeripheralInterruptDisable();
}

void APP_enableInterrupts() {
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
}

bool APP_eepromInitialized() {
    for (uint8_t i = 0; i < 8; i++) {
        if (DATAEE_ReadByte(i) != app_magic_num[i]) return false;
    }
    return true;
}

void APP_initializeEeprom() {
    for (uint8_t i = 0; i < 8; i++) {
        EEPROM_writeU8(ADDR_MAGIC + i, app_magic_num[i]);
    }

    EEPROM_writeU8(ADDR_SAMPLES, 3);

    for (uint8_t i = 0; i < API_MAX_SPOOLS; i++) {
        EEPROM_writeU32BE(SP_ADDR(i, ADDR_OFFSET), 0);
        EEPROM_writeU32BE(SP_ADDR(i, ADDR_SCALE), 1);
        EEPROM_writeU8(SP_ADDR(i, ADDR_STATE), 0);
    }
}

uint16_t APP_firstChar(char ch, char *str, uint16_t len) {
    for (uint16_t i = 0; i < len; i++) {
        if (str[i] == ch) return i;
    }
    return -1;
}

bool APP_isSpoolFlagSet(uint8_t spool, spool_state_flags flag) {
    return DATAEE_ReadByte(SP_ADDR(spool, ADDR_STATE)) & (uint8_t) flag;
}

void APP_setSpoolFlag(uint8_t spool, spool_state_flags flag, bool value) {
    uint16_t addr = SP_ADDR(spool, ADDR_STATE);
    uint8_t current = EEPROM_readU8(addr);
    if (value) {
        EEPROM_writeU8(addr, current | flag);
    } else {
        EEPROM_writeU8(addr, current ^ flag);
    }
}

bool APP_hx711Read() {
    if (active_spool == 0) return PORTAbits.RA0;
    if (active_spool == 1) return PORTAbits.RA2;
    if (active_spool == 2) return PORTAbits.RA6;
    if (active_spool == 3) return PORTBbits.RB0;
}

void APP_hx711Write(bool state) {
    if (active_spool == 0) LATAbits.LATA1 = state;
    if (active_spool == 1) LATAbits.LATA3 = state;
    if (active_spool == 2) LATAbits.LATA7 = state;
    if (active_spool == 2) LATBbits.LATB1 = state;
}


uint8_t APP_getSpoolsConnected() {
    uint8_t result = 0;
    for (uint8_t i = 0; i < API_MAX_SPOOLS; i++) {
        active_spool = i;
        pooling_active = 1;
        TMR0_StartTimer();
        while (pooling_active) {
            if (APP_hx711Read()) {
                result |= 1 << i;
                break;
            }
        }
    }
    return result;
}

void APP_timer0() {
    pooling_active = 0;
    TMR0_StopTimer();
}

void APP_activateSpool(uint8_t num) {
    active_spool = num;
    HX711_setOffset(EEPROM_readU32BE(SP_ADDR(num, ADDR_OFFSET)));
    HX711_setScale(EEPROM_readU32BE(SP_ADDR(num, ADDR_SCALE)));
}

