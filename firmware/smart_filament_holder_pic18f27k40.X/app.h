#ifndef APP_H
#define	APP_H

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "mcc_generated_files/device_config.h"

#define ADDR_MAGIC      0x000
#define ADDR_SAMPLES    0x008
#define ADDR_DELAY      0x009

#define ADDR_OFFSET     0x000
#define ADDR_SCALE      0x004
#define ADDR_INITIAL_W  0x008
#define ADDR_REF_W      0x00c
#define ADDR_STATE      0x010
#define ADDR_NAME       0x011

#define SP_ADDR(SPOOL, OFFSET) ((uint16_t)SPOOL * (uint16_t)0x80 + (uint16_t)0x80 + (uint16_t)OFFSET)

typedef enum {
    APP_SPOOL_HAS_REF_W = 0x1,
    APP_SPOOL_HAS_NAME = 0x2,

    APP_SPOOL_FLAG_3 = 0x4,
    APP_SPOOL_FLAG_4 = 0x8,
    APP_SPOOL_FLAG_5 = 0x10,
    APP_SPOOL_FLAG_6 = 0x20,
    APP_SPOOL_FLAG_7 = 0x40,
    APP_SPOOL_FLAG_8 = 0x80,
} spool_state_flags;

void APP_initialize(void);

void APP_startDevice();

void APP_UART0Write(char ch);

char APP_UART0Read();

void APP_switchESP(bool state);

void APP_replyToBroadcast(char* data, uint16_t len);

uint16_t APP_tcpEndpoint(char* data, uint16_t len, char **response);

uint8_t _APP_itoa(uint32_t value);

uint8_t _APP_itoaOffset(uint32_t value, uint16_t offset);

bool _APP_atoi(char *data, uint8_t start, uint8_t len, uint32_t *value);

uint8_t _APP_setMessage(char *message);

void APP_delayUS(uint16_t delay);

void APP_disableInterrupts();

void APP_enableInterrupts();

bool APP_eepromInitialized();

void APP_initializeEeprom();

bool APP_isSpoolFlagSet(uint8_t spool, spool_state_flags flag);

void APP_setSpoolFlag(uint8_t spool, spool_state_flags flag, bool value);

bool APP_hx711Read();

void APP_hx711Write(bool);

uint8_t APP_getSpoolsConnected();

void APP_timer0();

void APP_activateSpool(uint8_t num);

void APP_startConfigServerAP();

/*
0x000  -  0x07f  SYSTEM
0x080  -  0x0ff  SPOOL 0
0x100  -  0x17f  SPOOL 1
0x180  -  0x1ff  SPOOL 2
0x200  -  0x27f  SPOOL 3

SYSTEM:
  + 0x000 - 0x007 magic number (8)
  + 0x008         samples  (u8)
  + 0x009 - 0x00c delay    (u32)

SPOOL:
  + 0x000 - 0x003 offset (u32)
  + 0x004 - 0x007 scale (u32)
  + 0x008 - 0x00b initial weight (u32)
  + 0x00c - 0x00f ref weight (u32)
  + 0x010         state (u8)
  + 0x011 - 0x080 name

Spool states:
  & 0x1 = Has reference weight
Spool address: 0x80 + (spool_num) * 0x80

*/

#endif	/* APP_H */