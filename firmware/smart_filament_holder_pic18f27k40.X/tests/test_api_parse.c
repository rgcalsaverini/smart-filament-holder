#include <gtest/gtest.h>
#include "../tcp_api.h"

#ifdef API_MAX_SPOOLS
#undef API_MAX_SPOOLS
#define API_MAX_SPOOLS 5
#endif

void test_spool_only(const char* cmd, tcp_endpoint expected_ep, tcp_endpoint_method expected_m) {
    tcp_endpoint endpoint;
    tcp_endpoint_method method;
    uint32_t uint_args[4] = {0, 0, 0, 0};
    char str_arg[128];

    // Valid
    char cmd1[64];
    sprintf(cmd1, "%s 1", cmd);
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd1, strlen(cmd1));
    ASSERT_EQ(endpoint, expected_ep);
    ASSERT_EQ(method, expected_m);
    ASSERT_EQ(uint_args[0], 1u);

    // Trailing data
    char cmd2[64];
    sprintf(cmd2, "%s 1 _", cmd);
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd2, strlen(cmd2));
    ASSERT_EQ(endpoint, EP_INVALID_ARG);

    // Too large
    char cmd3[64];
    sprintf(cmd3, "%s 200", cmd);
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd3, strlen(cmd3));
    ASSERT_EQ(endpoint, EP_INVALID_ARG);
}

void test_empty(char* cmd, tcp_endpoint expected_ep, tcp_endpoint_method expected_m) {
    tcp_endpoint endpoint;
    tcp_endpoint_method method;
    uint32_t uint_args[4] = {0, 0, 0, 0};
    char str_arg[128];

    // Valid
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd, strlen(cmd));
    ASSERT_EQ(endpoint, expected_ep);
    ASSERT_EQ(method, expected_m);

    // Unexpected argument
    char cmd2[64];
    sprintf(cmd2, "%s 2", cmd);
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd2, strlen(cmd2));
    ASSERT_EQ(endpoint, EP_INVALID_ARG);
}

TEST(TCP_API_Parse, ParseIntValid) {
    uint16_t cmd_len = 0;
    bool error = false;
    uint32_t res;

    // Zero
    cmd_len = 0;
    res = _API_parseInteger(0, 0, "0", &cmd_len, 1, &error);
    ASSERT_EQ(res, 0);
    ASSERT_EQ(error, false);
    ASSERT_EQ(cmd_len, 1);

    // One digit
    cmd_len = 0;
    res = _API_parseInteger(0, 3, "1", &cmd_len, 1, &error);
    ASSERT_EQ(res, 1);
    ASSERT_EQ(error, false);
    ASSERT_EQ(cmd_len, 1);

    // Two digits
    cmd_len = 0;
    res = _API_parseInteger(2, 50, "21", &cmd_len, 2, &error);
    ASSERT_EQ(res, 21);
    ASSERT_EQ(error, false);
    ASSERT_EQ(cmd_len, 2);

    // Large
    cmd_len = 0;
    res = _API_parseInteger(4294967290, 4294967291, "4294967290", &cmd_len, 10, &error);
    ASSERT_EQ(res, 4294967290);
    ASSERT_EQ(error, false);
    ASSERT_EQ(cmd_len, 10);

    // Longer string
    cmd_len = 0;
    res = _API_parseInteger(0, 20, "15999999999999999999999999", &cmd_len, 2, &error);
    ASSERT_EQ(res, 15);
    ASSERT_EQ(error, false);
    ASSERT_EQ(cmd_len, 2);

    // Space at end
    cmd_len = 0;
    res = _API_parseInteger(0, 20, "4 5 6", &cmd_len, 5, &error);
    ASSERT_EQ(res, 4);
    ASSERT_EQ(error, false);
    ASSERT_EQ(cmd_len, 2);

    // Space at beginning
    cmd_len = 0;
    res = _API_parseInteger(0, 20, " 4", &cmd_len, 2, &error);
    ASSERT_EQ(res, 4);
    ASSERT_EQ(error, false);
    ASSERT_EQ(cmd_len, 2);
}

TEST(TCP_API_Parse, ParseIntInvalid) {
    uint16_t cmd_len = 0;
    bool error = false;

    // Smaller than min
    _API_parseInteger(1, 0, "0", &cmd_len, 1, &error);
    ASSERT_EQ(error, true);

    // Larger than max
    _API_parseInteger(0, 3, "4", &cmd_len, 1, &error);
    ASSERT_EQ(error, true);

    // Invalid ints
    _API_parseInteger(2, 50, "a12", &cmd_len, 3, &error);
    ASSERT_EQ(error, true);
    _API_parseInteger(2, 50, "12a", &cmd_len, 3, &error);
    ASSERT_EQ(error, true);
    _API_parseInteger(2, 50, "1.2", &cmd_len, 3, &error);
    ASSERT_EQ(error, true);

}

TEST(TCP_API_Parse, GetSpools) {
    test_empty("get spools", EP_SPOOLS, EP_GET);
}

TEST(TCP_API_Parse, GetSpoolState) {
    test_spool_only("get spool-state", EP_SPOOL_STATE, EP_GET);
}

TEST(TCP_API_Parse, GetSpoolName) {
    test_spool_only("get spool-name", EP_SPOOL_NAME, EP_GET);
}

TEST(TCP_API_Parse, GetSpoolRefWeight) {
    test_spool_only("get spool-ref-weight", EP_SPOOL_REF_WEIGHT, EP_GET);
}

TEST(TCP_API_Parse, GetLCValue) {
    test_spool_only("get lc-value", EP_LC_VALUE, EP_GET);
}
TEST(TCP_API_Parse, GetLCValueAverage) {
    char cmd[] = "get lc-value-average";
    tcp_endpoint endpoint;
    tcp_endpoint_method method;
    uint32_t uint_args[4] = {0, 0, 0, 0};
    char str_arg[128];

    // Valid
    char cmd1[64];
    sprintf(cmd1, "%s 0 6 0", cmd);
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd1, strlen(cmd1));
    ASSERT_EQ(endpoint, EP_LC_VALUE_AVERAGE);
    ASSERT_EQ(method, EP_GET);
    ASSERT_EQ(uint_args[0], 0u);
    ASSERT_EQ(uint_args[1], 6u);
    ASSERT_EQ(uint_args[2], 0u);

    // Trailing data
    char cmd2[64];
    sprintf(cmd2, "%s 0 6 0 9", cmd);
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd2, strlen(cmd2));
    ASSERT_EQ(endpoint, EP_INVALID_ARG);

    // Too small times
    char cmd3[64];
    sprintf(cmd3, "%s 0 0 0", cmd);
    API_parseRequest(&endpoint, &method, uint_args, str_arg, cmd3, strlen(cmd3));
    ASSERT_EQ(endpoint, EP_INVALID_ARG);
}

TEST(TCP_API_Parse, GetLCSamples) {
    test_empty("get lc-samples", EP_LC_SAMPLES, EP_GET);
}

TEST(TCP_API_Parse, GetLCOffset) {
    test_spool_only("get lc-offset", EP_LC_OFFSET, EP_GET);
}

TEST(TCP_API_Parse, GetLCScale) {
    test_spool_only("get lc-scale", EP_LC_SCALE, EP_GET);
}
