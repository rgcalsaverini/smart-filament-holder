#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/drivers/uart.h"
#include "external/pic8-libs/esp8266/iot.h"
#include "external/pic8-libs/esp8266/ap.h"
#include "app.h"

void main(void) {
    SYSTEM_Initialize();
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    APP_initialize();

    APP_startDevice();
//    APP_startConfigServerAP();

    while (1) {
        ESP8266_waitForClients();
    }
}