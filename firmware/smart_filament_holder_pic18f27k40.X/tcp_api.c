#include <xc.h>
#include <stdbool.h>
#include <string.h>
#include "tcp_api.h"

#define MAX_UINT32 4294967295

#define EPCMD(__CMD__) _API_hasSubstr(__CMD__, input + cmd_len, &cmd_len, len - cmd_len)

#define RECEIVE_NOTHING(__EP__)\
    *endpoint = __EP__; \
    if (cmd_len < len) error = ERR_TRAILING; \
    if (!error) return;

#define RECEIVE_ONE_INT(__EP__, __MIN__, __MAX__) \
    *endpoint = __EP__;                  \
    api_uint_args[0] = _API_parseInteger(__MIN__, __MAX__, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if (!error && cmd_len < len) error = ERR_TRAILING;\
    if (!error) return;

#define RECEIVE_TWO_INT(__EP__, __MIN1__, __MAX1__, __MIN2__, __MAX2__) \
    *endpoint = __EP__;                  \
    api_uint_args[0] = _API_parseInteger(__MIN1__, __MAX1__, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if (!error) api_uint_args[1] = _API_parseInteger(__MIN2__, __MAX2__, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if (!error && cmd_len < len) error = ERR_TRAILING;\
    if (!error) return;

#define RECEIVE_SPOOL_ID_ONLY(__EP__) RECEIVE_ONE_INT(__EP__, 0, API_MAX_SPOOLS-1)

#define RECEIVE_SPOOL_ID_PLUS_1_INT(__EP__, __MIN__) \
    *endpoint = __EP__;                                \
    api_uint_args[0] = _API_parseInteger(0, API_MAX_SPOOLS-1, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if(!error) api_uint_args[1] = _API_parseInteger(__MIN__, MAX_UINT32, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if(!error && (cmd_len < len))error = ERR_TRAILING;\
    if (!error) return;

#define RECEIVE_SPOOL_ID_PLUS_2_INT(__EP__) \
    *endpoint = __EP__;                                \
    api_uint_args[0] = _API_parseInteger(0, API_MAX_SPOOLS-1, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if(!error) api_uint_args[1] = _API_parseInteger(1, MAX_UINT32, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if(!error) api_uint_args[2] = _API_parseInteger(0, MAX_UINT32, input + cmd_len, &cmd_len, len - cmd_len, &error); \
    if(!error && cmd_len < len) error = ERR_TRAILING;\
    if (!error) return;

# define RECEIVE_SPOOL_ID_PLUS_STR(__EP__) \
        *endpoint = __EP__;                                \
        api_uint_args[0] = _API_parseInteger(0, API_MAX_SPOOLS-1, input + cmd_len, &cmd_len, len - cmd_len, &error); \
        if (!error) { \
            memcpy(api_str_arg, input + cmd_len, len - cmd_len);                                                   \
            api_uint_args[1] =  len - cmd_len;              \
            error = error || (len - cmd_len < 2);           \
            if (len - cmd_len < 2) error = ERR_STR_MIN;     \
            if (!error) return; \
        }


void API_parseRequest(tcp_endpoint *endpoint,
                      tcp_endpoint_method *method,
                      char *input,
                      uint16_t len) {

    uint16_t cmd_len = 0;
    tcp_endpoint_error error = ERR_NOTHING;

//    // Strip
//    while (!_API_valid_char(input[len - 1]) || input[len - 1] == ' ') {
//        len--;
//    }
//    while (!_API_valid_char(input[0]) || input[0] == ' ') {
//        input = input + 1;
//        len--;
//    }

    if (_API_hasSubstr("get", input, &cmd_len, len - cmd_len)) {
        *method = EP_GET;
        if (EPCMD("version")) { RECEIVE_NOTHING(EP_VERSION); }
        if (EPCMD("spools")) { RECEIVE_NOTHING(EP_SPOOLS); }
        else if (EPCMD("spool-state")) { RECEIVE_SPOOL_ID_ONLY(EP_SPOOL_STATE); }
        else if (EPCMD("spool-name")) { RECEIVE_SPOOL_ID_ONLY(EP_SPOOL_NAME); }
        else if (EPCMD("spool-ref-weight")) { RECEIVE_SPOOL_ID_ONLY(EP_SPOOL_REF_WEIGHT); }
        else if (EPCMD("spool-weight")) { RECEIVE_SPOOL_ID_ONLY(EP_SPOOL_WEIGHT); }
        else if (EPCMD("lc-value-average")) { RECEIVE_SPOOL_ID_PLUS_2_INT(EP_LC_VALUE_AVERAGE); }
        else if (EPCMD("lc-value")) { RECEIVE_SPOOL_ID_ONLY(EP_LC_VALUE); }
        else if (EPCMD("lc-samples")) { RECEIVE_NOTHING(EP_LC_SAMPLES); }
        else if (EPCMD("lc-offset")) { RECEIVE_SPOOL_ID_ONLY(EP_LC_OFFSET); }
        else if (EPCMD("lc-scale")) { RECEIVE_SPOOL_ID_ONLY(EP_LC_SCALE); }
        else if (EPCMD("eeprom")) { RECEIVE_TWO_INT(EP_EEPROM, 0, 1023, 0, 128); }
    } else if (_API_hasSubstr("set", input, &cmd_len, len - cmd_len)) {
        *method = EP_SET;
        if (EPCMD("spool-name")) { RECEIVE_SPOOL_ID_PLUS_STR(EP_SPOOL_NAME); }
        else if (EPCMD("spool-ref-weight")) { RECEIVE_SPOOL_ID_PLUS_1_INT(EP_SPOOL_REF_WEIGHT, 1); }
        else if (EPCMD("lc-samples")) { RECEIVE_ONE_INT(EP_LC_SAMPLES, 1, 20); }
        else if (EPCMD("lc-offset")) { RECEIVE_SPOOL_ID_PLUS_1_INT(EP_LC_OFFSET, 0); }
        else if (EPCMD("lc-scale")) { RECEIVE_SPOOL_ID_PLUS_1_INT(EP_LC_SCALE, 1); }
        else if (EPCMD("eeprom")) { RECEIVE_TWO_INT(EP_EEPROM, 0, 1023, 0, 255); }
    }
    else if (EPCMD("check")) { RECEIVE_NOTHING(EP_CHECK); }
    else if (EPCMD("convert")) { RECEIVE_SPOOL_ID_PLUS_1_INT(EP_CONVERT, 0); }
    else if (EPCMD("reset")) { RECEIVE_NOTHING(EP_RESET); }
    else if (EPCMD("int")) { RECEIVE_ONE_INT(EP_ECHO_INT, 0, MAX_UINT32); }

    if (error != ERR_NOTHING) {
        *endpoint = EP_INVALID_ARG;
        api_uint_args[0] = error;
        return;
    }

    *endpoint = EP_NON_EXISTENT;
}

bool _API_hasSubstr(const char *substr, const char *str, uint16_t *cmd_len, uint16_t max_len) {
    uint16_t i = 0;
    while (str[i] == ' ') i++;
    for (; substr[i] != 0; i++) {
        if (i >= max_len || substr[i] != str[i]) return false;
    }
    while (str[i] == ' ') i++;
    *cmd_len += i;
    return true;
}

uint32_t _API_parseInteger(uint32_t min,
                           uint32_t max,
                           const char *data,
                           uint16_t *cmd_len,
                           uint16_t max_len,
                           tcp_endpoint_error *err) {
    uint32_t value = 0;
    uint16_t i = 0;

    while (data[i] == ' ') i++;
    if (i >= max_len) {
        *err = ERR_INT_MISSING;
        return 0;
    }
    for (; i < max_len && data[i] != ' ' && data[i] != 0; i++) {
        if (data[i] < '0' || data[i] > '9') {
            *err = ERR_INT_CHAR;
            api_str_arg[0] = data[i];
            return 0;
        }
        value = value * 10 + (data[i] - '0');
    }
    if (min != 0 && value < min) {
        *err = ERR_INT_MIN;
        return 0;
    }
    if (max != MAX_UINT32 && value > max) {
        *err = ERR_INT_MAX;
        return 0;
    }
    while (data[i] == ' ') i++;
    *cmd_len += i;
    return value;
}

bool _API_atoi(const char *data, uint8_t start, uint8_t len, uint32_t *value) {
    *value = 0;
    for (uint8_t i = start; i < len && data[i] != ' '; i++) {
        if (data[i] < '0' || data[i] > '9') return false;
        *value = (*value) * 10 + (data[i] - '0');
    }
    return true;
}

bool _API_valid_char(char ch) {
    return (ch >= ' ' && ch <= '~');
}