#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# main configuration
CND_ARTIFACT_DIR_main=dist/main/production
CND_ARTIFACT_NAME_main=smart_filament_holder_pic18f27k40.X.production.hex
CND_ARTIFACT_PATH_main=dist/main/production/smart_filament_holder_pic18f27k40.X.production.hex
CND_PACKAGE_DIR_main=${CND_DISTDIR}/main/package
CND_PACKAGE_NAME_main=smartfilamentholderpic18f27k40.x.tar
CND_PACKAGE_PATH_main=${CND_DISTDIR}/main/package/smartfilamentholderpic18f27k40.x.tar
# test configuration
CND_ARTIFACT_DIR_test=dist/test/production
CND_ARTIFACT_NAME_test=smart_filament_holder_pic18f27k40.X.production.hex
CND_ARTIFACT_PATH_test=dist/test/production/smart_filament_holder_pic18f27k40.X.production.hex
CND_PACKAGE_DIR_test=${CND_DISTDIR}/test/package
CND_PACKAGE_NAME_test=smartfilamentholderpic18f27k40.x.tar
CND_PACKAGE_PATH_test=${CND_DISTDIR}/test/package/smartfilamentholderpic18f27k40.x.tar
