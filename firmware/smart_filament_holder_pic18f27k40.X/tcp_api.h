#ifndef TCP_API_H
#define TCP_API_H
#define API_MAX_SPOOLS 4

#include <stdint.h>

typedef enum {
    EP_NON_EXISTENT,
    EP_INVALID_ARG,
    EP_SPOOLS,
    EP_SPOOL_STATE,
    EP_SPOOL_NAME,
    EP_SPOOL_REF_WEIGHT,
    EP_SPOOL_WEIGHT,
    EP_LC_VALUE,
    EP_LC_VALUE_AVERAGE,
    EP_LC_SAMPLES,
    EP_LC_OFFSET,
    EP_LC_SCALE,
    EP_CHECK,
    EP_CONVERT,
    EP_RESET,
    EP_ECHO_INT,
    EP_VERSION,
    EP_EEPROM,
} tcp_endpoint;

typedef enum {
    EP_GET,
    EP_SET,
} tcp_endpoint_method;

typedef enum {
    ERR_NOTHING = 0u,
    ERR_STR_MIN,
    ERR_INT_MIN,
    ERR_INT_MAX,
    ERR_INT_CHAR,
    ERR_INT_MISSING,
    ERR_TRAILING,
} tcp_endpoint_error;

/**
 * get version
 * get spools
 * get spool-state <int : spool_id>
 * get spool-name <int : spool_id>
 * get spool-ref-weight <int : spool_id>
 * get lc-value <int : spool_id>
 * get lc-value-average <int : spool_id> <int : times> <int : delay>
 * get lc-samples
 * get lc-offset <int : spool_id>
 * get lc-scale <int : spool_id>
 *
 * set spool-name <str: name>
 * set spool-ref-weight <int : spool_id> <int : ref_w>
 * set lc-samples <int : samples>
 * set lc-offset <int : spool_id> <int : offset>
 * set lc-scale <int : spool_id> <int : scale>
 *
 * convert <int: spool_id> <int: value>
 * check
 * reset
 *
 * @param endpoint
 * @param method
 * @param uint_args
 * @param str_arg
 * @param input
 * @param len
 */

uint32_t api_uint_args[5];

char api_str_arg[128];

void API_parseRequest(tcp_endpoint *endpoint, tcp_endpoint_method *method, char *input, uint16_t len);

bool _API_hasSubstr(const char *substr, const char *str, uint16_t *cmd_len, uint16_t max_len);

uint32_t _API_parseInteger(uint32_t min, uint32_t max, const char *str, uint16_t *cmd_len, uint16_t max_len, tcp_endpoint_error *err);

bool _API_atoi(const char *data, uint8_t start, uint8_t len, uint32_t *value);

bool _API_valid_char(char ch);

#endif //TCP_API_H
